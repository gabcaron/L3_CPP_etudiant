#ifndef __FIB_HPP
#define __FIB_HPP

int fibonacciRecursif(int x);
int fibonacciIteratif(int x);

#endif
