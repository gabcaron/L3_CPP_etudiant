#ifndef __VEC_HPP
#define __VEC_HPP

struct Vecteur3{
	
		float X, Y, Z;

		void afficher();
		float norme();
		Vecteur3 addition(Vecteur3 v, Vecteur3 v2);
		float produitScalaire(Vecteur3 v);
	
};

Vecteur3 creerVecteur(float X, float Y, float Z);
void afficher(Vecteur3 v);

#endif
