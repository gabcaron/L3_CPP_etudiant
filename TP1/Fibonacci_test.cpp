// Fibonacci_test.cpp
#include "fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibonacci) {};

TEST(GroupFibonacci, test_fibonacci_1) { // Test fibonacciRecursif
	int result = fibonacciRecursif(6);
	CHECK_EQUAL(8, result);
}

TEST(GroupFibonacci, test_fibonacci_2) { // Test fibonacciIteratif
	int result = fibonacciIteratif(3);
	CHECK_EQUAL(2, result);
}
