//Vecteur3.cpp
#include "Vecteur3.hpp"
#include <iostream>
#include <math.h>

using namespace std;

Vecteur3 creerVecteur(float X, float Y, float Z){
	Vecteur3 v;
	
	v.X = X;
	v.Y = Y;
	v.Z = Z;
	
	return v;
}

void afficher(Vecteur3 v){
	cout << "( " << v.X << " , " << v.Y << " , " << v.Z << " )" << endl;
}

void Vecteur3::afficher(){
	cout << "( " << X << " , " << Y << " , " << Z << " )" << endl;
}

float Vecteur3::norme(){
	return sqrt(X*X + Y*Y + Z*Z);
}

Vecteur3 Vecteur3::addition(Vecteur3 v, Vecteur3 v2){
	v.X += v2.X;
	v.Y += v2.Y;
	v.Z += v2.Z;
	
	return v;
}

float Vecteur3::produitScalaire(Vecteur3 v){
	return sqrt(pow(v.X - X, 2) + pow(v.Y - Y, 2) + pow(v.Z - Z, 2));
}
