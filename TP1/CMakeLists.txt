cmake_minimum_required( VERSION 3.0 )
project( TP_vide )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra" )

find_package( PkgConfig REQUIRED )
pkg_check_modules( PKG_CPPUTEST REQUIRED cpputest )
include_directories( ${PKG_CPPUTEST_INCLUDE_DIRS} )

# programme principal
add_executable( main main.cpp Fibonacci.cpp Vecteur3.cpp )
target_link_libraries( main )

# programme de test
add_executable( main_test main_test.cpp Fibonacci_test.cpp Fibonacci.cpp )
target_link_libraries( main_test ${PKG_CPPUTEST_LIBRARIES} )
