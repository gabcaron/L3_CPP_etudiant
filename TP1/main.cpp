// main.cpp
#include <iostream>
#include "fibonacci.hpp"
#include "Vecteur3.hpp"
int main() {
	//std::cout << "Hello world !" << std::endl;
	std::cout << "FibonacciRecursif 7e terme : " << fibonacciRecursif(7) << std::endl;
	std::cout << "FibonacciIteratif 7e terme : " << fibonacciIteratif(7) << std::endl;
	
	Vecteur3 v = creerVecteur(2, 3, 6);
	v.afficher();
	
	std::cout << "Norme : " << v.norme() << std::endl;
	
	Vecteur3 v2 = creerVecteur(1, 1, 1);
	v = v.addition(v, v2);
	v.afficher();
	
	std::cout << "Produit scalaire : " << v.produitScalaire(creerVecteur(1, 1, 1)) << std::endl;

	return 0;
}
