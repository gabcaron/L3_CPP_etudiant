// Fibonacci.cpp
#include "fibonacci.hpp"
int fibonacciRecursif(int x) {
	if(x==0 || x==1)
		return x;
	else
		return fibonacciRecursif(x-1) + fibonacciRecursif(x-2);
}

int fibonacciIteratif(int x) {
	int n1=0, n2=1, n3, i;
	for(i=2; i<=x; i++) {
		n3 = n1 + n2;
		n1 = n2;
		n2 = n3;
	}
	return n2;
}
