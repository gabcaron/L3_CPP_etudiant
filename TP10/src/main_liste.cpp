#include <iostream>

#include "Liste.hpp"
#include "ListeGenerique.h"
#include "Personne.h"

int main() {
    std::cout << "Liste :" << std::endl;
    Liste l1;
    l1.push_front(37);
    l1.push_front(13);
    std::cout << l1 << std::endl;

    std::cout << "Liste Generique :" << std::endl;
    ListeGenerique<float> l2;
    l2.push_front(37.57);
    l2.push_front(13.74);
    std::cout << l2 << std::endl;

    std::cout << "Liste Personne :" << std::endl;
    Personne p1, p2;
    p1._nom = "Fab";
    p1._age = 14;
    p2._nom = "Erick";
    p2._age = 20;
    ListeGenerique<Personne> l3;
    l3.push_front(p1);
    l3.push_front(p2);
    std::cout << l3 << std::endl;

    return 0;
}

