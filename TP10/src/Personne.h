//
// Created by gab on 19/05/2020.
//

#ifndef TP10_PERSONNE_H
#define TP10_PERSONNE_H

#include <string>
#include <ostream>

struct Personne {
    std::string _nom;
    int _age;
};

std::ostream& operator<<(std::ostream& os, const Personne& p) {
    return os << p._nom << " " << p._age;
}


#endif //TP10_PERSONNE_H
