//
// Created by gab on 19/05/2020.
//

#ifndef TP10_LISTEGENERIQUE_H
#define TP10_LISTEGENERIQUE_H

#include <cassert>
#include <ostream>

template <typename T>

// liste d'entiers avec itérateur
class ListeGenerique {
private:
    struct Noeud {
        T _valeur;
        Noeud *_ptrNoeudSuivant;
    };

protected:
    Noeud *_ptrTete;

public:
    class iterator {
    protected:
        Noeud *_ptrNoeudCourant;

    public:
        iterator(Noeud* ptrNoeudCourant){
            this->_ptrNoeudCourant = ptrNoeudCourant;
        }

        const iterator & operator++() {
            _ptrNoeudCourant = _ptrNoeudCourant->_ptrNoeudSuivant;
            return *this;
        }

        T& operator*() const {
            return _ptrNoeudCourant->_valeur;
        }

        bool operator!=(const iterator &iter) const {
            return _ptrNoeudCourant != iter._ptrNoeudCourant;
        }

        friend ListeGenerique;
    };

public:
    ListeGenerique() {
        this->_ptrTete = nullptr;
    }

    ~ListeGenerique() {
        clear();
        delete _ptrTete;
    }

    void push_front(T valeur) {
        Noeud *noeud = new Noeud;
        noeud->_valeur = valeur;
        noeud->_ptrNoeudSuivant = _ptrTete;
        this->_ptrTete = noeud;
    }

    T& front() const {
        return _ptrTete->_valeur;
    }

    void clear() {
        while(_ptrTete != nullptr) {
            Noeud *noeud = _ptrTete;
            _ptrTete = _ptrTete->_ptrNoeudSuivant;
            delete noeud;
        }
    }

    bool empty() const {
        if(_ptrTete == nullptr) {
            return true;
        }
        return false;
    }

    iterator begin() const {
        return _ptrTete;
    }

    iterator end() const {
        return nullptr;
    }

};

template <typename T>

std::ostream& operator<<(std::ostream& os, const ListeGenerique<T>& l) {
    auto iter = l.begin();
    while(iter != l.end()) {
        os << *iter << " ";
        ++iter;
    }
    return os;
}

#endif //TP10_LISTEGENERIQUE_H
