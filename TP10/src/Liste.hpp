
#ifndef LISTE_HPP_
#define LISTE_HPP_

#include <cassert>
#include <ostream>

// liste d'entiers avec itérateur
class Liste {
    private:
        struct Noeud {
            int _valeur;
            Noeud *_ptrNoeudSuivant;
        };

    protected:
        Noeud *_ptrTete;

    public:
        class iterator {
            protected:
                Noeud *_ptrNoeudCourant;

            public:
                iterator(Noeud* ptrNoeudCourant){
                    this->_ptrNoeudCourant = ptrNoeudCourant;
                }

                const iterator & operator++() {
                    _ptrNoeudCourant = _ptrNoeudCourant->_ptrNoeudSuivant;
                    return *this;
                }

                int& operator*() const {
                    return _ptrNoeudCourant->_valeur;
                }

                bool operator!=(const iterator &iter) const {
                    return _ptrNoeudCourant != iter._ptrNoeudCourant;
                }

                friend Liste; 
        };

    public:
        Liste() {
            this->_ptrTete = nullptr;
        }

        ~Liste() {
            clear();
            delete _ptrTete;
        }

        void push_front(int valeur) {
            Noeud *noeud = new Noeud;
            noeud->_valeur = valeur;
            noeud->_ptrNoeudSuivant = _ptrTete;
            this->_ptrTete = noeud;
        }

        int& front() const {
            return _ptrTete->_valeur;
        }

        void clear() {
            while(_ptrTete != nullptr) {
                Noeud *noeud = _ptrTete;
                _ptrTete = _ptrTete->_ptrNoeudSuivant;
                delete noeud;
            }
        }

        bool empty() const {
            if(_ptrTete == nullptr) {
                return true;
            }
            return false;
        }

        iterator begin() const {
            return _ptrTete;
        }

        iterator end() const {
            return nullptr;
        }

};

std::ostream& operator<<(std::ostream& os, const Liste& l) {
    auto iter = l.begin();
    while(iter != l.end()) {
        os << *iter << " ";
        ++iter;
    }
    return os;
}

#endif

