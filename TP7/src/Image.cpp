//
// Created by gab on 09/04/2020.
//

#include <iostream>
#include <fstream>
#include "Image.h"
#include <math.h>

Image::Image(int largeur, int hauteur) {
    _largeur = largeur;
    _hauteur = hauteur;
    _pixels = new int[_largeur * _hauteur];
    for(int i=0; i<_largeur * _hauteur; i++) {
        _pixels[i] = i;
    }
}

Image::Image(Image const & img) {
    _largeur = img.getLargeur();
    _hauteur = img.getHauteur();
    _pixels = new int [_largeur * _hauteur];
    for(int i=0; i<_largeur; i++) {
        for(int j=0; j<_hauteur; j++) {
            setPixel(i, j, img.getPixel(i, j));
        }
    }
}

Image::~Image() {
    delete [] _pixels;
}

int Image::getLargeur() const {
    return _largeur;
}

int Image::getHauteur() const {
    return _hauteur;
}

int Image::getPixel(int i, int j) const {
    return _pixels[_largeur * i + j];
}

void Image::setPixel(int i, int j, int couleur) {
    _pixels[_largeur * i + j] = couleur;
}

int &Image::getPixelByReference(int i, int j) {
    return _pixels[_largeur * i + j];
}

Image& Image::operator=(const Image &im) {
    _largeur = im.getLargeur();
    _hauteur = im.getHauteur();
    delete[] _pixels;
    _pixels = new int [_largeur * _hauteur];
    for(int i=0; i<_largeur; i++) {
        for(int j=0; j<_hauteur; j++) {
            setPixel(i, j, im.getPixel(i ,j));
        }
    }
    return *this;
}

void ecrirePnm(const Image & img, const std::string & nomFichier) {
    std::ofstream fichier(nomFichier);
    if(fichier.is_open()) {
        fichier << "P2 " << img.getLargeur() << " " << img.getHauteur() << " 255" << std::endl;
        for(int i=0; i<img.getLargeur(); i++) {
            for(int j=0; j<img.getHauteur(); j++) {
                fichier << img.getPixel(i, j) << " ";
            }
            fichier << std::endl;
        }
        fichier.close();
    } else {
        std::cout << "Erreur : ouverture du fichier impossible" << std::endl;
    }
}

void remplir(Image & img) {
    for(int i=0; i<img.getLargeur(); i++) {
        int color = ((cos(i)+1)/2)*255;
        for(int j=0; j<img.getHauteur(); j++) {
            img.setPixel(j, i, color);
        }
    }
}

Image bordure(const Image & img, int couleur, int epaisseur) {
    Image im(img);
    for(int i=0; i<img.getLargeur(); i++) {
        for(int j=0; j<epaisseur; j++) {
            im.setPixel(i, j, couleur);
        }
        for(int j=im.getHauteur()-epaisseur; j<im.getHauteur(); j++) {
            im.setPixel(i, j, couleur);
        }
    }
    for(int i=0; i<im.getHauteur(); i++) {
        for(int j=0; j<epaisseur; j++) {
            im.setPixel(j, i, couleur);
        }
        for(int j=im.getLargeur()-epaisseur; j<im.getLargeur(); j++) {
            im.setPixel(j, i, couleur);
        }
    }
    return im;
}