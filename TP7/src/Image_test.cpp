//
// Created by gab on 09/04/2020.
//

#include "Image.h"
#include <CppUTest/CommandLineTestRunner.h>

//void MemoryLeakWarningPlugin::turnOffNewDeleteOverloads() {};

TEST_GROUP(GroupImage) { };

TEST(GroupImage, test_img) {
    Image im(3,3);
    CHECK_EQUAL(im.getLargeur(), 3);
    CHECK_EQUAL(im.getHauteur(), 3);
    im.setPixel(1, 1, 10);
    CHECK_EQUAL(im.getPixel(1,1),10);
    im.setPixel(2,2,20);
    int &tmp = im.getPixelByReference(2,2);
    CHECK_EQUAL(tmp, 20);
    tmp = 50;
    CHECK_EQUAL(tmp, 50);
}
