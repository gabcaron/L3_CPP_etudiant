//
// Created by gab on 09/04/2020.
//

#include <iostream>
#include "Image.h"

int main() {
    Image im(2,2);
    std::cout << "Largeur : " << im.getLargeur() << std::endl;
    std::cout << "Hauteur : " << im.getHauteur() << std::endl;
    im.setPixel(0, 0, 10);
    im.setPixel(1, 0, 20);
    im.setPixel(0, 1, 30);
    im.setPixel(1, 1, 40);
    std::cout << "Pixel [0,0] : " << im.getPixel(0, 0) << std::endl;

    Image im2(10, 10);
    remplir(im2);
    Image im3 = bordure(im2, 0, 1);
    ecrirePnm(im3, "image.pnm");
    return 0;
}