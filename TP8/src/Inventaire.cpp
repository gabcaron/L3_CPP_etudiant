#include "Inventaire.hpp"
#include <vector>

std::ostream &operator<<(std::ostream &os, const Inventaire &i) {
    for(const Bouteille b : i._bouteilles) {
        os << b;
    }
    return os;
}
