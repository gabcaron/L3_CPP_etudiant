#include "Controleur.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

Controleur::Controleur(int argc, char ** argv) {
    _vues.push_back(std::make_unique<VueConsole>(*this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));

    //_inventaire._bouteilles.push_back(Bouteille{"cyanure", "2013-08-18", 0.25});

    chargerInventaire();

    for (auto & v : _vues)
      v->actualiser();
}

std::string Controleur::getTexte() {
    std::ostringstream ostringstream;
    ostringstream << _inventaire;
    return ostringstream.str();
}

void Controleur::chargerInventaire() {
    _inventaire._bouteilles.push_back(Bouteille{"Maredsous", "2013-08-18", 0.75});
    for (auto & v : _vues)
        v->actualiser();
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}


