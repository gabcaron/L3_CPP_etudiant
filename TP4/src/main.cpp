//
// Created by gab on 24/03/2020.
//

#include <iostream>
#include "Couleur.h"
#include "Point.h"
#include "Ligne.h"
#include "PolygoneRegulier.h"
#include <vector>

int main() {

    Couleur c (1.0, 0.0, 0.0);
    Point p0 (0, 0);
    Point p1 (100, 200);

    Ligne l (c, p0, p1);
    l.afficher();

    PolygoneRegulier poly (Couleur(0, 1, 0), Point(100, 200), 10 ,4);
    poly.afficher();

    std::vector<FigureGeometrique *> figGeos = {
            new Ligne (Couleur(2.0, 0.0, 0.0), Point(0, 0), Point(100, 200)),
            new PolygoneRegulier(Couleur(2.0, 1.0, 0.0), Point(1, 1), 2, 4)
    };

    for(FigureGeometrique *figGeo : figGeos) {
        figGeo->afficher();
    }

    return 0;
}