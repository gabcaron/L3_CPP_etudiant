//
// Created by gab on 24/03/2020.
//

#ifndef TP4_COULEUR_H
#define TP4_COULEUR_H

struct Couleur {
    double _r;
    double _g;
    double _b;

    Couleur();
    Couleur(double r, double g, double b) : _r(r), _g(g), _b(b) {}
};

#endif //TP4_COULEUR_H
