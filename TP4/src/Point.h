//
// Created by gab on 24/03/2020.
//

#ifndef TP4_POINT_H
#define TP4_POINT_H

struct Point {
    int _x;
    int _y;

    Point() : _x(0), _y(0) {}
    Point(int x, int y) : _x(x), _y(y) {}
};

#endif //TP4_POINT_H
