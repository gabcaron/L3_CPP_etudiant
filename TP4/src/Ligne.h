//
// Created by gab on 24/03/2020.
//

#ifndef TP4_LIGNE_H
#define TP4_LIGNE_H

#include "Couleur.h"
#include "Point.h"
#include "FigureGeometrique.h"

class Ligne : public FigureGeometrique {
private:
    Point _p0;
    Point _p1;

public:
    Ligne(const Couleur &couleur, const Point &p0, const Point &p1);
    virtual void afficher() const override ;
    const Point &getP0() const;
    const Point &getP1() const;

};

#endif //TP4_LIGNE_H
