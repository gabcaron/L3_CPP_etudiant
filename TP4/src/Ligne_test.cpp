//
// Created by gab on 24/03/2020.
//

#include <CppUTest/CommandLineTestRunner.h>
#include "Couleur.h"
#include "Ligne.h"

TEST_GROUP(GroupLigne) { };

TEST(GroupLigne, test_lig) {
    Ligne l (Couleur(10, 0, 50), Point(0, 0), Point(200, 400));
    CHECK_EQUAL(Point(0, 0)._x, l.getP0()._x);
    CHECK_EQUAL(Point(0, 0)._y, l.getP0()._y);
    CHECK_EQUAL(Point(200, 400)._x, l.getP1()._x);
    CHECK_EQUAL(Point(200, 400)._y, l.getP1()._y);
};