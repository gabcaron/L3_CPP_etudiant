//
// Created by gab on 24/03/2020.
//

#ifndef TP4_FIGUREGEOMETRIQUE_H
#define TP4_FIGUREGEOMETRIQUE_H

#include "Couleur.h"

class FigureGeometrique {
protected:
    Couleur _couleur;

public:
    FigureGeometrique(const Couleur &couleur);
    const Couleur &getCouleur() const;
    virtual void afficher() const;

};

#endif //TP4_FIGUREGEOMETRIQUE_H
