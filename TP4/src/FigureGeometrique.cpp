//
// Created by gab on 24/03/2020.
//

#include "FigureGeometrique.h"
#include "Couleur.h"

FigureGeometrique::FigureGeometrique(const Couleur &couleur) :_couleur(couleur) {}

const Couleur &FigureGeometrique::getCouleur() const {
    return _couleur;
}

void FigureGeometrique::afficher() const {}