//
// Created by gab on 24/03/2020.
//

#include <CppUTest/CommandLineTestRunner.h>

int main(int argc, char ** argv) {
    return CommandLineTestRunner::RunAllTests(argc, argv);
}