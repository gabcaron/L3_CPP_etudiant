//
// Created by gab on 24/03/2020.
//

#include <CppUTest/CommandLineTestRunner.h>
#include "Couleur.h"
#include "Point.h"
#include "PolygoneRegulier.h"

TEST_GROUP(GroupPolygoneRegulier) { };

TEST(GroupPolygoneRegulier, test_poly) {
    PolygoneRegulier poly (Couleur(50.0, 0.0, 0.0), Point(0, 100), 10, 4);

    CHECK_EQUAL(Couleur(50.0, 0.0, 0.0)._r, poly.getCouleur()._r);
    CHECK_EQUAL(Couleur(50.0, 0.0, 0.0)._g, poly.getCouleur()._g);
    CHECK_EQUAL(Couleur(50.0, 0.0, 0.0)._b, poly.getCouleur()._b);

    CHECK_EQUAL(4, poly.getNbPoints());
    
}