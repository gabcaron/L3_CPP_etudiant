#include "Inventaire.hpp"
#include <vector>
#include <iostream>
#include <sstream>

std::ostream &operator<<(std::ostream &os, const Inventaire &i) {
    for(const Bouteille b : i._bouteilles) {
        os << b;
    }
    return os;
}

std::istream &operator>>(std::istream &is, Inventaire &i) {
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
    std::string bf;
    Bouteille b;

    while (std::getline(is, bf, '\n')) {
        std::istringstream istring(bf);
        istring >> b;
        i._bouteilles.push_back(b);
    }
    std::locale::global(vieuxLoc);
    return is;
}