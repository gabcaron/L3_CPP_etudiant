//
// Created by gab on 23/03/2020.
//

#ifndef TP_VIDE_PRODUIT_H
#define TP_VIDE_PRODUIT_H

#include <string>

class Produit {
private:
    int _id;
    std::string _description;

public:
    Produit(int id, const std::string &description);
    int getId() const;
    const std::string &getDescription() const;
    void afficherProduit() const;
};

#endif //TP3_PRODUIT_H
