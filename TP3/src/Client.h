//
// Created by gab on 23/03/2020.
//

#ifndef TP_VIDE_CLIENT_H
#define TP_VIDE_CLIENT_H

#include <string>

class Client {
private:
    int _id;
    std::string _nom;

public:
    Client(int id, const std::string &nom);
    int getId() const;
    std::string getNom() const;
    void afficherClient() const;
};

#endif //TP3_CLIENT_H
