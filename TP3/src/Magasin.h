//
// Created by gab on 23/03/2020.
//

#ifndef TP_VIDE_MAGASIN_H
#define TP_VIDE_MAGASIN_H

#include <vector>
#include "Client.h"
#include "Produit.h"
#include "Location.h"

class Magasin {
private:
    std::vector<Client> _clients;
    std::vector<Produit> _produits;
    std::vector<Location> _locations;
    int _idCourantClient;
    int _idCourantProduit;

public:
    Magasin();

    const int nbClients();
    void ajouterClient(const std::string &nom);
    const void afficherClients();
    void supprimerClient(int idClient);

    const int nbProduits();
    void ajouterProduit(const std::string &nom);
    const void afficherProduits();
    void supprimerProduit(int idProduit);

    const int nbLocation();
    void ajouterLocation(int idClient, int idProduit);
    const void afficherLocations();
    void supprimerLocation(int idClient, int idProduit);
    const bool trouverClientDansLocation(int idClient);
    const std::vector<int> calculerClientsLibres();
    const bool trouverProduitDansLocation(int idProduit);
    const std::vector<int> calculerProduitsLibres();
};

#endif //TP3_MAGASIN_H
