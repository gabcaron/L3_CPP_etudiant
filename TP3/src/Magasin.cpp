//
// Created by gab on 23/03/2020.
//

#include <iostream>
#include "Magasin.h"
#include "Client.h"

Magasin::Magasin() {
    _idCourantClient = 0;
    _idCourantProduit = 0;
}

const int Magasin::nbClients() {
    return _clients.size();
}

void Magasin::ajouterClient(const std::string &nom) {
    _clients.push_back(Client(_idCourantClient, nom));
    _idCourantClient++;
}

const void Magasin::afficherClients() {
    for(size_t i = 0; i < Magasin::nbClients(); ++i) {
        _clients[i].Client::afficherClient();
    }
}

void Magasin::supprimerClient(int idClient) {
    try {
        if(idClient <= Magasin::nbClients() -1) {
            for(size_t i = idClient; i < Magasin::nbClients() - 1; ++i) {
                std::swap(_clients[i], _clients[i+1]);
            }
            _clients.pop_back();
        } else {
            throw std::string("Client inexistant");
        }
    }
    catch(std::string const &chaine) {
        std::cout << chaine << std::endl;
    }
}

const int Magasin::nbProduits() {
    return _produits.size();
}

void Magasin::ajouterProduit(const std::string &nom) {
    _produits.push_back(Produit(_idCourantProduit, nom));
    _idCourantProduit++;
}

const void Magasin::afficherProduits() {
    for(size_t i = 0; i < Magasin::nbProduits(); ++i) {
        _produits[i].Produit::afficherProduit();
    }
}

void Magasin::supprimerProduit(int idProduit) {
    try {
        if(idProduit < Magasin::nbProduits() - 1) {
            for(size_t i = idProduit; i < Magasin::nbProduits() - 1; ++i) {
                std::swap(_produits[i], _produits[i+1]);
            }
            _produits.pop_back();
        } else {
            throw std::string("Produit inexistant");
        }
    }
    catch (std::string const &chaine) {
        std::cout << chaine << std::endl;
    }
}

const int Magasin::nbLocation() {
    return _locations.size();
}

void Magasin::ajouterLocation(int idClient, int idProduit) {
    try {
        Location l (idClient, idProduit);
        if(Magasin::nbLocation() != 0) {
            for(size_t i = 0; i <= Magasin::nbLocation() - 1; ++i) {
                if(l._idProduit == _locations[i]._idProduit && l._idClient == _locations[i]._idProduit) {
                    throw std::string("Location inexistante");
                }
            }
            _locations.push_back(l);
        } else {
            _locations.push_back(l);
        }
    }
    catch(std::string const &chaine) {
        std::cout << chaine << std::endl;
    }
}

const void Magasin::afficherLocations() {
    for(size_t i = 0; i < Magasin::nbLocation() - 1; ++i) {
        _locations[i].Location::afficherLocation();
    }
}

void Magasin::supprimerLocation(int idClient, int idProduit) {
    try {
        Location l (idClient, idProduit);
        for(size_t i = 0; i < Magasin::nbLocation() - 1; ++i) {
            if(l._idProduit == _locations[i]._idProduit && l._idClient == _locations[i]._idProduit) {
                std::swap(_locations[i], _locations[i+1]);
            }
        }
        _locations.pop_back();
    }
    catch(std::string const &chaine) {
        std::cout << "Localisation inexistante" << std::endl;
    }
}

const bool Magasin::trouverClientDansLocation(int idClient) {
    for(size_t i = 0; i <= Magasin::nbLocation() + 1; ++i) {
        if(_locations[i]._idClient == idClient) {
            return true;
        }
    }
    return false;
}

const std::vector<int> Magasin::calculerClientsLibres() {
    std::vector<int> cpt;
    for(size_t i = 0; i < Magasin::nbClients() - 1; ++i) {
        if(!trouverClientDansLocation(_clients[i].getId())) {
            cpt.push_back(_clients[i].getId());
        }
    }
    return cpt;
}

const bool Magasin::trouverProduitDansLocation(int idProduit) {
    for(size_t i = 0; i <= Magasin::nbLocation() - 1; ++i) {
        if(_locations[i]._idProduit == idProduit) {
            return true;
        }
    }
    return false;
}

const std::vector<int> Magasin::calculerProduitsLibres() {
    std::vector<int> cpt;
    for(size_t i = 0; i < Magasin::nbProduits() - 1; ++i) {
        if(!trouverProduitDansLocation(_produits[i].getId())) {
            cpt.push_back(_produits[i].getId());
        }
    }
    return cpt;
}