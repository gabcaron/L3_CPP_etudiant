//
// Created by gab on 23/03/2020.
//

#ifndef TP_VIDE_LOCATION_H
#define TP_VIDE_LOCATION_H

struct Location {
    int _idClient;
    int _idProduit;

    Location();
    Location(int idClient, int idProduit);
    void afficherLocation() const;
};

#endif //TP3_LOCATION_H
