//
// Created by gab on 23/03/2020.
//

#include <iostream>
#include "Client.h"

Client::Client(int id, const std::string &nom) {
    this->_id = id;
    this->_nom = nom;
}

int Client::getId() const {
    return _id;
}

std::string Client::getNom() const {
    return _nom;
}

void Client::afficherClient() const {
    std::cout << "Client (" << _id << ", " << _nom << ")" << std::endl;
}