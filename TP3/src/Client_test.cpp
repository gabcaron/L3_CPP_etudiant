//
// Created by gab on 23/03/2020.
//

#include "Client.h"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupClient) { };

TEST(GroupClient, test_cli) {
    Client c (3, "tutu");

    CHECK_EQUAL(3, c.getId());
    CHECK_EQUAL("tutu", c.getNom());
}