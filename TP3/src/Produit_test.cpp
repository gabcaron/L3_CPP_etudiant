//
// Created by gab on 23/03/2020.
//

#include "Produit.h"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupProduit) { };

TEST(GroupProduit, test_prod) {
    Produit p (7, "tata");

    CHECK_EQUAL(7, p.getId());
    CHECK_EQUAL("tata", p.getDescription());
}