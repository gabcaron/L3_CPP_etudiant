//
// Created by gab on 23/03/2020.
//

#include "Produit.h"
#include "Magasin.h"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupMagasin) { };

TEST(GroupMagasin, test_mag) {
    Magasin m;

    m.ajouterClient("toto");
    m.ajouterClient("tata");
    m.ajouterClient("tutu");

    CHECK_EQUAL(3, m.nbClients());

    m.supprimerClient(1);

    CHECK_EQUAL(2, m.nbClients());
    CHECK_THROWS(std::string, m.supprimerClient(3));

    m.ajouterProduit("PC");
    m.ajouterProduit("Stylo");
    m.ajouterProduit("sac");
    m.ajouterProduit("Télé");

    CHECK_EQUAL(4, m.nbProduits());

    m.supprimerProduit(2);

    CHECK_EQUAL(3, m.nbProduits());
    CHECK_THROWS(std::string, m.supprimerProduit(3));

    m.ajouterLocation(0,3);
    m.ajouterLocation(2, 1);

    CHECK_THROWS(std::string, m.ajouterLocation(2, 1));
    CHECK_EQUAL(2, m.nbLocation());

    m.supprimerLocation(0, 3);

    CHECK_EQUAL(1, m.nbLocation());
    CHECK_TRUE(m.trouverClientDansLocation(2));
    CHECK_TRUE(m.trouverProduitDansLocation(1));

    std::vector<int> nbCli = m.calculerClientsLibres();

    CHECK_EQUAL(1, nbCli.size());

    std::vector<int> nbProd = m.calculerProduitsLibres();

    CHECK_EQUAL(1, nbProd.size());

}