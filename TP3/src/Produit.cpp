//
// Created by gab on 23/03/2020.
//

#include <iostream>
#include "Produit.h"

Produit::Produit(int id, const std::string &description) {
    this->_id = id;
    this->_description = description;
}

int Produit::getId() const {
    return _id;
}

const std::string &Produit::getDescription() const {
    return _description;
}

void Produit::afficherProduit() const {
    std::cout << "Produit (" << _id << ", " << _description << ")" <<std::endl;
}