//
// Created by gab on 23/03/2020.
//

#include <iostream>
#include "Location.h"
#include "Client.h"
#include "Produit.h"
#include "Magasin.h"

int main() {

    Location l;
    l.afficherLocation();

    Client c (42,"toto");
    c.afficherClient();

    Produit p (42, "toto");
    p.afficherProduit();

    Magasin m;
    m.ajouterClient("toto");
    m.ajouterClient("tutu");
    std::cout << "Nombre de clients : " << m.nbClients() << std::endl;
    m.afficherClients();
    m.supprimerClient(1);
    std::cout << "Nombre de clients : " << m.nbClients() << std::endl;
    m.ajouterProduit("PC");
    m.ajouterProduit("Stylo");
    m.ajouterProduit("Télé");
    std::cout << "Nombre de produits : " << m.nbProduits() << std::endl;
    m.afficherProduits();
    m.supprimerProduit(2);
    std::cout << "Nombre de produits : " << m.nbProduits() << std::endl;
    m.ajouterLocation(0,1);
    m.ajouterLocation(1,3);
    std::cout << "Nombre de locations : " << m.nbLocation() << std::endl;
    m.afficherLocations();
    m.supprimerLocation(1,1);
    std::cout << "Nombre de locations : " << m.nbLocation() << std::endl;
    std::cout << m.trouverClientDansLocation(0) << std::endl;
    std::cout << m.trouverProduitDansLocation(1) << std::endl;
    std::cout << m.trouverClientDansLocation(3) << std::endl;
    std::cout << m.trouverProduitDansLocation(4) << std::endl;

    return 0;
}