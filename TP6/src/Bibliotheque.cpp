//
// Created by gab on 08/04/2020.
//

#include "Bibliotheque.h"

#include <iostream>
#include <algorithm>
#include <string>
#include <fstream>

void Bibliotheque::afficher() const {
    for(const Livre &l : *this) {
        std::cout << l << std::endl;
    }
}

void Bibliotheque::trierParAuteurEtTitre() {
    std::sort(this->begin(), this->end());
}

void Bibliotheque::trierParAnnee() {
    std::sort(this->begin(), this->end(), [](Livre l1, Livre l2) {
       return l1.getAnnee() < l2.getAnnee();
    });
}

void Bibliotheque::ecrireFichier(const std::string &nomFichier) const {
    std::ofstream fichier(nomFichier);
    if(fichier.is_open()) {
        for(const Livre &l : *this) {
            fichier << l << std::endl;
        }
        fichier.close();
    } else {
        std::cout << "Erreur : ouverture du fichier impossible" << std::endl;
    }
}

void Bibliotheque::lireFichier(const std::string &nomFichier) {
    std::ifstream fichier(nomFichier);
    std::string ligne;
    std::string delimiter = ";";
    if(fichier.is_open()) {
        while(getline(fichier, ligne)) {
            size_t position = 0;
            std::string elements[2];
            while((position = ligne.find(delimiter)) != std::string::npos) {
                if(elements->size() == 0) {
                    elements[0] = ligne.substr(0, position);
                } else {
                    elements[elements->size()-1] = ligne.substr(0, position);
                }
                ligne.erase(0, position++);
            }
            this->push_back(Livre(elements[0], elements[1], std::stoi(ligne)));
        }
        fichier.close();
    } else {
        std::cout << "Erreur : lecture du fichier impossible" << std::endl;
    }
}