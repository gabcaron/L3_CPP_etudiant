
#include <iostream>
#include "Livre.h"
#include "Bibliotheque.h"

int main() {
    Bibliotheque b;
    b.push_back(Livre("titre", "auteur", 2020));
    b.push_back(Livre("toto", "tata", 2019));
    b.ecrireFichier("Bibliotheque.txt");
    b.lireFichier("Bibliotheque.txt");
    b.afficher();
    return 0;
}

