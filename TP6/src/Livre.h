//
// Created by gab on 27/03/2020.
//

#ifndef TP6_LIVRE_H
#define TP6_LIVRE_H

#include <string>
#include <ostream>

class Livre {

private:
    std::string _titre;
    std::string _auteur;
    int _annee;
public:
    Livre();
    Livre(const std::string &titre, const std::string &auteur, int annee);
    const std::string &getTitre() const;
    const std::string &getAuteur() const;
    int getAnnee() const;
    bool operator<(const Livre &livre2) const;
    bool operator==(const Livre &livre2) const;
    friend std::ostream &operator<<(std::ostream &os, const Livre &livre);
    friend std::istream &operator>>(std::istream &is, Livre &livre);
};

#endif //TP6_LIVRE_H
