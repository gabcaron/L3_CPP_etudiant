//
// Created by gab on 27/03/2020.
//

#include <iostream>
#include <string>
#include "Livre.h"

Livre::Livre() : _titre("LicenceInfo"), _auteur("Gabin"), _annee(2020) {}

Livre::Livre(const std::string &titre, const std::string &auteur, int annee) {
    std::size_t found = titre.find(";");
    if(found != std::string::npos) {
        throw std::string("Erreur : Caractère interdit dans le titre (point-virgule)");
    }

    found = titre.find("\r");
    if(found != std::string::npos) {
        throw std::string("Erreur : Caractère interdit dans le titre (retour chariot)");
    }

    found = auteur.find(";");
    if(found != std::string::npos) {
        throw std::string("Erreur : Caractère interdit dans l'auteur (point-virgule)");
    }

    found = auteur.find("\r");
    if(found != std::string::npos) {
        throw std::string("Erreur : Caractère interdit dans l'auteur (retour chariot");
    }

    _titre = titre;
    _auteur = auteur;
    _annee = annee;
}

const std::string & Livre::getTitre() const {
    return _titre;
}

const std::string & Livre::getAuteur() const {
    return _auteur;
}

int Livre::getAnnee() const {
    return _annee;
}

bool Livre::operator<(const Livre &livre2) const {
    if(this->getAuteur() == livre2.getAuteur()) {
        return this->getTitre() < livre2.getTitre();
    } else {
        return this->getAuteur() < livre2.getAuteur();
    }
}

bool Livre::operator==(const Livre &livre2) const {
    return this->getTitre() == livre2.getTitre() && this->getAuteur() == livre2.getAuteur() && this->getAnnee() == livre2.getAnnee();
}

std::ostream &operator<<(std::ostream &os, const Livre &livre) {
    os << livre._titre << ";" << livre._auteur << ";" << livre._annee;
    return os;
}

std::istream &operator>>(std::istream &is, Livre &livre) {
    char str[99];
    is.getline(str, 99, ';');
    livre._titre = str;
    is.getline(str, 99, ';');
    livre._auteur = str;
    is.getline(str, 99, ';');
    livre._annee = std::atoi(str);
    return is;
}