//Liste.cpp
#include "Liste.hpp"
#include <iostream>

using namespace std;

Liste::Liste(){
    _tete = nullptr;
}

Liste::~Liste(){
    Noeud* prev;
    Noeud* suiv = _tete;
    while(suiv != nullptr){
        prev = suiv;
        suiv = suiv->_suivant;
        delete prev;
    }
}

void Liste::ajouterDevant(int valeur){
    Noeud* _n = new Noeud();
    _n->_valeur = valeur;
    _n->_suivant = _tete;
    _tete = _n;
}

int Liste::getTaille() const {
    int cpt = 0;
    if(_tete != nullptr){
        Noeud* n = _tete;
        while(n != nullptr){
            n = n->_suivant;
            cpt++;
        }
    }
    return cpt;
}

int Liste::getElement(int indice) const {
    Noeud* n = _tete;
    int i = 0;
    while(i != indice && i <= getTaille()){
        n = n->_suivant;
        i++;
    }

    return n->_valeur;
}