#include <iostream>
#include "Liste.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupListe){ };

TEST(GroupListe, Liste_Constructeur){
        Liste l = Liste();
        CHECK_EQUAL(l._tete, 0);
}

TEST(GroupListe, Liste_ajouterDevant){
    Liste l = Liste();
    l.ajouterDevant(10);
    CHECK_EQUAL(l._tete->_valeur, 10);
}

TEST(GroupListe, Liste_getTaille){
    Liste l = Liste();
    l.ajouterDevant(1);
    l.ajouterDevant(2);
    l.ajouterDevant(3);
    CHECK_EQUAL(l.getTaille(), 3);
}

TEST(GroupListe, Liste_getElement){
    Liste l = Liste();
    l.ajouterDevant(1);
    l.ajouterDevant(5);
    l.ajouterDevant(10);
    l.ajouterDevant(3);
    CHECK_EQUAL(l.getElement(3), 10);
}