#include "Doubler.hpp"

#include <iostream>

int main() {
    std::cout << doubler(21) << std::endl;
    std::cout << doubler(21.f) << std::endl;

    int a;
    a = 42;
    std::cout << "a : " << a << std::endl;
    int* p;
    p = &a;
    *p = 37;
    std::cout << "a : " << a <<std::endl;

    int* t = new int[10];
    t[2] = 42;
    std::cout << "3e case de t : " << t[2] << std::endl;
    delete [] t;
    t = nullptr;

    return 0;
}

